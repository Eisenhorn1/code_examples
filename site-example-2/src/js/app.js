//Плавный скролл
(function($scroller) {
    if (!$scroller.length) {
        return;
    }

    $scroller.each(function() {
        let scroller = new Scroller($(this));
        scroller.init();
    });
})($('.j-scroller'));

// кнопка вызова меню на мобильных устройствах
(function($burger) {
    if (!$burger.length) {
        return;
    }

    $burger.on('click', function() {
        let $wrapper = $(this).closest('.j-menu');
        let isFixed  = 'is-fixed';
        let $nav     = $wrapper.find('.b-menu__nav');
        let $btnList = $wrapper.find('.b-menu__btn-list-wrapper');
        let $bottom  = $wrapper.find('.b-menu__bottom');
        $burger.toggleClass(isFixed);
        $wrapper.toggleClass(isFixed);
        $nav.slideToggle();
        $btnList.toggle();
        $bottom.toggle();
    })
})($('.j-hamburger'));
