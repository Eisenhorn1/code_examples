define('app/brick-hover-effects', ['jquery', 'vimeo'], function($) {
    //ЭФФЕКТ ПАРАЛЛАКСА - .j-bg-animate
    (function($animate) {
        if (!$animate.length) {
            return;
        }

        $(document).on('mousemove', '.j-bg-animate', function(e) {
            let w = $(this).width();
            let h = $(this).height();
            let relX = e.clientX - $(this).offset().left; //движение влево-вправо.
            let relY = e.clientY - $(this).offset().top; //движение вверх-вниз.

            // Offset для того, чтобы картинка не гуляла по всему блоку
            let x = 100 + ((relX / w) * 10) + '%'; // 100 - прибиваем к правому краю
            let y = 50 + ((relY / h) * 10) + '%'; // 100 - прибиваем по центру

            $(this).css({
                'background-position-x': x,
                'background-position-y': y
            });
        });

        $(document).on('mouseout', '.j-bg-animate', function() {
            $(this).css({
                'background-position-x': 'right',
                'background-position-y': 'center'
            });
        });
    })($('.j-bg-animate'));

    //ВИДЕО БЭКГРАУНДОМ - .j-bg-video
    (function($video) {
        if (!$video.length) {
            return;
        }
        /*global $f:true*/
        let froogaloop = $f($video[0]);
        const $bgImage = $video.next();

        $(document).on('mouseenter', '.j-bg-video-neighbor', function() {
            $video.css('opacity', '1');
            froogaloop.api('play');
            //сеттаймаут для того, чтобы видео успело начать воспроизводиться
            setTimeout(function() {
                $bgImage.css('opacity', '0');
            }, 300);
        });

        $(document).on('mouseleave', '.j-bg-video-neighbor', function() {
            $bgImage.css('opacity', '1');
            froogaloop.api('unload');
            $video.css('opacity', '0');
        });
    })($('.j-bg-video'));

    //ГАЛЕРЕЯ ПРИ ХОВЕРЕ - .j-bg-gallery
    (function($gallery) {
        if (!$gallery.length) {
            return;
        }

        const images = $gallery.data('images');

        $(images).each(function(i, image) {
            $gallery.append(`<div class="b-brick__bg-gallery-img j-bg-gallery-item"
                                  style="background-image: url('${image}')"/>`);
        });

        const $slides = $gallery.find('.j-bg-gallery-item');

        function fadeSliding() {
            $($slides.get().reverse()).each(function(i, item) {
                $(item).css({
                    'animation-delay': i * 1 + 's'
                });
            });
        }

        fadeSliding();
    })($('.j-bg-gallery'));

    (function($link) {
        if (!$link.length) {
            return;
        }

        let $content = $('.b-video-popup');
        let $bricks  = $('.b-bricks');
        let active   = 'is-active';
        let blurred  = 'is-blurred-bg';

        $(document).on('click', '.j-open-overlay', function(e) {
            e.preventDefault();
            let target          = $(this).data('target');
            let targetContainer = $content.filter($(`#${target}`));
            let $video          = targetContainer.find('.j-vimeo');
            let froogaloop      = $f($video[0]);
            targetContainer.addClass(active);
            $bricks.addClass(blurred);
            froogaloop.api('play');
        });

        $(document).on('click', '.b-popup__close', function() {
            let $video     = $(this).next('.j-vimeo');
            let froogaloop = $f($video[0]);
            $content.removeClass(active);
            $bricks.removeClass(blurred);
            froogaloop.api('unload');
            froogaloop.api('pause');
        });
    })($('.j-open-overlay'));
});

